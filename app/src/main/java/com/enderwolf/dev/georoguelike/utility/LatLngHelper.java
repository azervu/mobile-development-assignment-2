package com.enderwolf.dev.georoguelike.utility;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by !Tulingen on 27.09.2014.
 * Helper math class converting latitude lognitude to meters offset from map center
 */
public class LatLngHelper {

    private static final double EARTH_CIRCUMFERENCE = 40075.017;

    public static LatLng addLatKM (LatLng latLng, double northSouth) {
        double newLat = LatLngHelper.kmToLat(northSouth);

        newLat += latLng.latitude;
        newLat = (newLat > 90.0) ? 90.0 : newLat;
        newLat = (newLat <  0.0) ?  0.0 : newLat;

        return new LatLng(newLat,latLng.longitude);
    }

    public static LatLng addLngKM (LatLng latLng, double eastWest) {
        double newLng = LatLngHelper.kmToLng(eastWest, latLng.latitude);

        newLng += latLng.longitude;
        newLng = (newLng >  180.0) ? newLng - 360.0 : newLng;
        newLng = (newLng < -180.0) ? newLng + 360.0 : newLng;

        return new LatLng(latLng.latitude, newLng);
    }

    private static double kmToLat(double km) {
        return (360 * km) / EARTH_CIRCUMFERENCE;
    }

    private static double kmToLng(double km, double lat) {
        double calcLat = Math.toRadians(lat);
        return (360 * km) / (EARTH_CIRCUMFERENCE * Math.cos(calcLat));
    }

    public static double latToKM (double lat) {
        return (lat * EARTH_CIRCUMFERENCE) / 360;
    }

    public static double lngToKM (double lng, double lat) {
        double calcLat = Math.toRadians(lat);
        return (lng * EARTH_CIRCUMFERENCE * Math.cos(calcLat)) / 360;
    }
}
