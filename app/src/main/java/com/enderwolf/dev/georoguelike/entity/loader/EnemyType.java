package com.enderwolf.dev.georoguelike.entity.loader;

/**
 * Created by !Tulingen on 30.09.2014.
* This is the class for game enemy types.
 */
public class EnemyType extends EntityType {

    private final int health;
    private final int mana;
    private final int armor;
    private final int damage;

    public EnemyType (String name, int id, int imageId, int health, int mana, int armor, int damage) {
        super(name, id, imageId);

        this.health = health;
        this.mana = mana;
        this.armor = armor;
        this.damage = damage;
    }

    public int getHealth() {
        return this.health;
    }

    public int getMana() {
        return this.mana;
    }

    public int getArmor() {
        return this.armor;
    }

    public int getDamage() {
        return this.damage;
    }
}
