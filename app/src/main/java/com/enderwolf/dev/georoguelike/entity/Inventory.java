package com.enderwolf.dev.georoguelike.entity;

import com.enderwolf.dev.georoguelike.entity.loader.ItemLoader;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Vector;

/**
 * Created by !Tulingen on 30.09.2014.
 * Contains inventory data and manages the carry limit.
 */
@SuppressWarnings("ALL")
public class Inventory implements Serializable {

    private int carryWeightCap;
    private int currentWeight;
    private final Vector<Item> content;

    public Inventory () {
        this.content = new Vector<>(20);
        this.carryWeightCap = 20000;
        this.currentWeight = 0;
    }

    public Inventory (int carryWeightCap) {
        this.content = new Vector<>(20);
        this.carryWeightCap = carryWeightCap;
        this.currentWeight = 0;
    }

    public Inventory (Item ... items) {
        this.content = new Vector<>(Arrays.asList(items));
        this.carryWeightCap = this.currentWeight = Inventory.calculateWeight((Item []) content.toArray());
    }

    public Inventory (int carryWeightCap, Item ... items) {
        this.carryWeightCap = carryWeightCap;
        this.content = new Vector<>(Arrays.asList(items));
        this.currentWeight = Inventory.calculateWeight((Item []) content.toArray());
    }

    public Vector<Item> getList() {
        return content;
    }

    /**
     * Return item at index
     * @param index postion of item in vector
     * @return the item at index or null if it does not exist
     */
    public Item get(int index) {
        return (index < this.content.size() && index >= 0) ? this.content.get(index) : null;
    }

    public int getCurrentWeight() {
        return currentWeight;
    }

    public int getCarryWeightCap() {
        return carryWeightCap;
    }

    /**
     * Sets the carry weight for this inventory
     * @param carryWeightCap The weight capacity
     * @return true if possible, false if the new max is smaller than current weight
     */
    public boolean setCarryWeightCap(int carryWeightCap) {
        if(this.currentWeight > carryWeightCap) {
            return false;
        }

        this.carryWeightCap = carryWeightCap;
        return true;
    }

    /**
     * Tries to add the item to the players inventory
     * @param item the item to add
     * @return true if successful, false if full
     */
    public boolean add (Item item) {

        if(!this.isSpaceFor(item)) {
            return false;
        }

        boolean entryFound = false;
        for(Item i : this.content) {
            if(i.getId() == item.getId()) {
                i.addToStackSize(item.getStackSize());
                entryFound = true;
            }
        }

        this.currentWeight += Inventory.calculateWeight(item);

        if (!entryFound) {
            this.content.add(item);
        }

        return true;
    }

    /**
     * Tries to remove the specified amount of items
     * @param itemId the type of items to remove
     *  @param amount the number of items to remove
     */
    public void removeItem (int itemId, int amount) {
        for(Item i : this.content) {
            if(i.getId() == itemId) {
                if (i.removeFromStackSize(amount)) {
                    if (i.getStackSize() == 0) {
                        this.content.remove(i);
                    }
                }

            }
        }

        this.currentWeight -= Inventory.calculateWeight(new Item(itemId, 1));

    }

    boolean isSpaceFor(Item item) {
        return (ItemLoader.GetItemLoader().getEntityType(item.getId()).getWeight() * item.getStackSize() + this.currentWeight < this.carryWeightCap);
    }

    private static int calculateWeight(Item... items) {
        int weight = 0;

        for (Item i : items) {
            weight += ItemLoader.GetItemLoader().getEntityType(i.getId()).getWeight() * i.getStackSize();
        }

        return weight;
    }
}
