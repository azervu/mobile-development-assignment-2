package com.enderwolf.dev.georoguelike.utility;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.enderwolf.dev.georoguelike.R;
import com.enderwolf.dev.georoguelike.entity.Item;
import com.enderwolf.dev.georoguelike.entity.loader.ItemLoader;
import com.enderwolf.dev.georoguelike.entity.loader.ItemType;

import java.util.List;

/**
 * Created by !Tulingen on 30.09.2014.
 * A class generating the UI information for the invetory
 */

public class ItemAdapter extends BaseAdapter {

    private final List<Item> items;

    public ItemAdapter (List<Item> items) {
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();

        GridLayout itemView = new GridLayout(context);
        Item item = this.items.get(position);
        ItemLoader loader = ItemLoader.GetItemLoader();
        ItemType type = loader.getEntityType(item.getId());

        ImageView image = new ImageView(context);
        image.setImageResource(type.getImageId()); // TODO per item icon
        image.setPadding(3, 3, 3, 3);


        GridLayout.LayoutParams imageParams = new GridLayout.LayoutParams(
                GridLayout.spec(0,2, GridLayout.CENTER),
                GridLayout.spec(0,1, GridLayout.CENTER)
        );

        itemView.addView(image, imageParams);


        TextView name = new AlwaysMarqueeTextView(context);
        name.setTextAppearance(context, android.R.style.TextAppearance_DeviceDefault_Medium);

        StringBuilder nameString = new StringBuilder();
        nameString.append(type.getName());
        nameString.append(" x ");
        nameString.append(item.getStackSize());

        name.setText(nameString);
        name.setPadding(3, 3, 3, 3);
        name.setSingleLine();
        name.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        name.setMarqueeRepeatLimit(-1);
        name.setHorizontallyScrolling(true);
        name.setWidth(parent.getWidth() - parent.getResources().getDrawable(type.getImageId()).getCurrent().getIntrinsicWidth());

        GridLayout.LayoutParams nameParams = new GridLayout.LayoutParams(
                GridLayout.spec(0,1, GridLayout.CENTER),
                GridLayout.spec(1,1, GridLayout.LEFT)
        );

        itemView.addView(name, nameParams);


        TextView desc = new TextView(context);
        desc.setTextAppearance(context, android.R.style.TextAppearance_DeviceDefault_Small);

        StringBuilder descString = new StringBuilder();
        descString.append(parent.getResources().getString(R.string.weight));
        descString.append(": ");
        descString.append(type.getWeight() * item.getStackSize());
        descString.append("g (");
        descString.append(type.getWeight());
        descString.append("g x ");
        descString.append(item.getStackSize());
        descString.append(")\n");
        descString.append(parent.getResources().getString(R.string.value));
        descString.append(": ");
        descString.append(type.getValue());
        descString.append("\n");
        if( type.getAddHp() != 0 ) {
            descString.append(parent.getResources().getString(R.string.heals));
            descString.append(": ");
            descString.append(type.getAddHp());
            descString.append("\n");
        }



        desc.setText(descString);
        desc.setPadding(3, 3, 3, 3);
        desc.setMinLines(1);
        desc.setMaxLines(6);


        GridLayout.LayoutParams descParams = new GridLayout.LayoutParams(
                GridLayout.spec(1,1, GridLayout.CENTER),
                GridLayout.spec(1,1, GridLayout.LEFT)
        );


        itemView.addView(desc, descParams);



        itemView.requestLayout();

        return itemView;
    }


    @SuppressWarnings("ALL")
    public class AlwaysMarqueeTextView extends TextView {

        public AlwaysMarqueeTextView(Context context) {
            super(context);
        }

        public AlwaysMarqueeTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public AlwaysMarqueeTextView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        public boolean isFocused() {
            return true;
        }
    }

}
