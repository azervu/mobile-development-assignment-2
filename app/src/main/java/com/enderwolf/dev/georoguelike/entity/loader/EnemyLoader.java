package com.enderwolf.dev.georoguelike.entity.loader;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

import com.enderwolf.dev.georoguelike.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Loader class for EnemyTypes
 * Created by !Tulingen on 30.09.2014.
 */
public class EnemyLoader extends EntityLoader<EnemyType> {

    private static EnemyLoader ENEMY_LOADER = null;

    public static void InitEnemyLoader(Resources resources, Context context) {
        ENEMY_LOADER = new EnemyLoader(resources, context);
    }

    public static EnemyLoader GetEnemyLoader() {
        if(!IsInit()) {
            Log.e("EnemyLoader.GetEnemyLoader", "function called before init");
            throw new NullPointerException("ENEMY_LOADER uninitialized");
        }

        return ENEMY_LOADER;
    }

    private static boolean IsInit () {
        return (ENEMY_LOADER != null);
    }

    private EnemyLoader(Resources resources, Context context) {
        super(resources, context);
    }

    @Override
    protected String getEntityName() {
        return "enemy";
    }

    @Override
    protected XmlResourceParser getXmlResourceParser(Resources resources) {
        return resources.getXml(R.xml.enemy_types);
    }

    @Override
    protected void loadEntityType(XmlPullParser parser, Resources resources, Context context) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, getEntityName());
        parser.next();

        String name = "";
        int id = 0;
        int imageId = -1;

        int health = 0;
        int mana = 0;
        int armor = 0;
        int damage = 0;


        while (!parser.getName().equals(getEntityName())) {
            String tag = parser.getName();
            Log.v("EnemyLoader.loadEntityType", "Tag: " + parser.getName());

            switch (tag) {
                case "id":
                    id = resources.getIdentifier(parseField(parser), "string", context.getPackageName());
                    name = resources.getString(id);
                break;

                case "image":
                    imageId = resources.getIdentifier(parseField(parser), "drawable", context.getPackageName());
                break;

                case "health":
                    health = Integer.parseInt(parseField(parser));
                break;

                case "mana":
                    mana = Integer.parseInt(parseField(parser));
                break;

                case "armor":
                    armor = Integer.parseInt(parseField(parser));
                break;

                case "damage":
                    damage = Integer.parseInt(parseField(parser));
                break;

                default:
                    Log.e("EnemyLoader.loadEntityType", "unhandled field: " + parser.getName());
                break;
            }
        }

        if (id == 0) {
            throw new IOException("Id field missing");
        }

        addEntity(id, new EnemyType(name, id, imageId, health, mana, armor, damage));
    }
}
