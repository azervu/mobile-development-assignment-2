package com.enderwolf.dev.georoguelike;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.enderwolf.dev.georoguelike.entity.IContent;
import com.enderwolf.dev.georoguelike.entity.Terrain;
import com.enderwolf.dev.georoguelike.utility.IUpdate;

import java.util.List;
import java.util.Random;

/**
 * This fragment displays information about the current square and provides the button for exploring
 * it.
 *
 */
public class ExploreFragment extends Fragment implements IUpdate {

    private OnContentListener lootListener;
    private Terrain currentSquare = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Button exploreButton;
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        exploreButton = (Button) view.findViewById(R.id.explore_button);
        exploreButton.setOnClickListener(new ExploreButtonListener());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        this.update();
    }

    private class ExploreButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            Log.d("ExploreButtonListener", "Explore button pushed");

            if (currentSquare != null) {

                currentSquare.setVisited(true);
                List<IContent> contentList = currentSquare.getContent();

                if(contentList.size() == 0) {
                    ((GameActivity) getActivity()).updateFragments();
                    return;
                }

                Random r = new Random();
                int index = r.nextInt(contentList.size());
                IContent content = contentList.get(index);
                IContent.InteractResult result = lootListener.onContentFound(content);

                if(result.getResult()) {
                    contentList.remove(index);
                }

                currentSquare.getEventText().add(result.getMessage());
            }

            update();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            lootListener = (OnContentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnLootFoundListener");
        }
    }

    public void setCurrentSquare(Terrain terrain) {
        this.currentSquare = terrain;
        this.update();
    }

    @Override
    public void update() {
        if(!this.isResumed() || this.currentSquare == null) {
            return;
        }

        this.currentSquare = ((GameActivity) this.getActivity()).getCurrentSquare();

        StringBuilder typeString = new StringBuilder();
        typeString.append((this.currentSquare.getVisited()) ? this.getString(currentSquare.getTypeId()) : "???");
        typeString.append(" [");
        typeString.append(currentSquare.getContent().size());
        typeString.append("]");

        //noinspection ConstantConditions
        ((TextView) this.getView().findViewById(R.id.terrain_type_text)).setText(typeString);
        ListView list = (ListView) this.getView().findViewById(R.id.explore_event_list);
        list.setAdapter(
            new ArrayAdapter<>(
                this.getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                this.currentSquare.getEventText()
            )
        );
    }

    public interface OnContentListener {
        public IContent.InteractResult onContentFound(IContent content);
    }
}
