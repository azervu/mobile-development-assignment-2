package com.enderwolf.dev.georoguelike;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.enderwolf.dev.georoguelike.entity.Inventory;
import com.enderwolf.dev.georoguelike.entity.Player;
import com.enderwolf.dev.georoguelike.entity.Stats;
import com.enderwolf.dev.georoguelike.utility.IUpdate;
import com.enderwolf.dev.georoguelike.utility.ItemAdapter;

/**
 * Created by !Tulingen on 24.09.2014.
 * This Fragment serves as the UI for the player stats and inventory.
 */
public class PlayerFragment extends Fragment implements IUpdate {

    private Player player = null;

    public PlayerFragment () {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.player = ((GameActivity) this.getActivity()).getPlayer();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_player, container, false);
    }

    @Override
    public void onResume() {
        this.update();
        super.onResume();
    }

    @Override
    public void update() {
        if(!this.isResumed() || this.player == null) {
            return;
        }

        this.player = ((GameActivity) this.getActivity()).getPlayer();

        View root = getView();

        Stats stats = this.player.getStats();

        @SuppressWarnings("ConstantConditions") TextView hpNumber = (TextView) root.findViewById(R.id.hp_number);
        hpNumber.setText(String.valueOf(stats.getHealth()));

        ProgressBar hpBar= (ProgressBar) root.findViewById(R.id.hp_bar);
        hpBar.setProgress(stats.getHealth());

        TextView manaNumber = (TextView) root.findViewById(R.id.mana_number);
        manaNumber.setText(String.valueOf(stats.getMana()));

        ProgressBar manaBar = (ProgressBar) root.findViewById(R.id.mana_bar);
        manaBar.setProgress(stats.getMana());

        Inventory inventory = this.player.getInventory();

        ((TextView) root.findViewById(R.id.carry_current)).setText(String.valueOf(inventory.getCurrentWeight()));
        ((TextView) root.findViewById(R.id.carry_max)).setText(String.valueOf(inventory.getCarryWeightCap()));

        final ListView inventoryView = (ListView) root.findViewById(R.id.inventory_scroll_view);
        inventoryView.removeAllViewsInLayout();

        inventoryView.setAdapter(
                new ItemAdapter(inventory.getList())
        );

        inventoryView.setOnItemClickListener((GameActivity) this.getActivity());
    }
}