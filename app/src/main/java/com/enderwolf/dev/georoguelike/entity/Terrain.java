package com.enderwolf.dev.georoguelike.entity;

import com.enderwolf.dev.georoguelike.utility.GridMap;
import com.enderwolf.dev.georoguelike.utility.Point;

import java.io.Serializable;
import java.util.Vector;

/**
 * Created by azervu on 23.09.2014.
 * A specific instance of terrain, contains a reference to its terrain type with is typeId to the
 * TerrainLoader singleton
 */
@SuppressWarnings("ALL")
public class Terrain implements Serializable, GridMap.Node {

    private boolean visited;
    private final int typeId;
    private final Vector<IContent> content;
    private final Vector<String> eventText;
    private final Point position;

    public Terrain(Point position, int typeId) {
        this(position, false, typeId);
    }

    public Terrain(Point position, boolean visited, int typeId) {
        this.position = position;
        this.visited = visited;
        this.typeId = typeId;

        this.content = new Vector<>();
        this.eventText = new Vector<>();
    }

    public boolean getVisited() {
        return this.visited;
    }

    public void setVisited( boolean visited) {
        this.visited = visited;
    }
    
    public int getTypeId() {
        return typeId;
    }

    public Vector<IContent> getContent() {
        return content;
    }

    public Vector<String> getEventText() {
        return this.eventText;
    }

    @Override
    public Point getPosition() {
        return this.position;
    }
}
