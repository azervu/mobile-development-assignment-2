package com.enderwolf.dev.georoguelike.entity.loader;

import com.enderwolf.dev.georoguelike.utility.LootTable;

/**
 * Created by !Tulingen on 24.09.2014.
 * This class stores data about specific types of terrain found in the game.
 * Actual instances of terrain objects hold an id reference to to their specific itemtype through the
 * TerrainLoader singleton.
 */
public class TerrainType extends EntityType {

    private final LootTable lootTable;

    TerrainType(String name, int id, int imageId, LootTable lootTable) {
        super(name, id, imageId);

        this.lootTable = lootTable;
    }

    public LootTable.LootDrop getRandomDrop() {
        return  lootTable.getRandomDrop();
    }
}