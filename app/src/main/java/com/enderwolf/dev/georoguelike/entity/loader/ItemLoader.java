package com.enderwolf.dev.georoguelike.entity.loader;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

import com.enderwolf.dev.georoguelike.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by azervu on 23.09.2014.
 * This class is a singleton that loads and stores item data.
 */
public class ItemLoader extends EntityLoader<ItemType> {
    private static ItemLoader ITEM_LOADER = null;

    public static void InitItemLoader(Resources resources, Context context) {
        ITEM_LOADER = new ItemLoader(resources, context);
    }

    public static ItemLoader GetItemLoader() {
        if(!IsInit()) {
            Log.e("ItemLoader.GetItemLoader", "function called before init");
            throw new NullPointerException("itemLoader uninitialized");
        }
        return ITEM_LOADER;
    }

    private static boolean IsInit () {
        return (ITEM_LOADER != null);
    }

    private ItemLoader(Resources resources, Context context) {
        super(resources, context);
    }

    @Override
    protected String getEntityName() {
        return "item";
    }

    @Override
    protected XmlResourceParser getXmlResourceParser(Resources resources) {
        return resources.getXml(R.xml.item_types);
    }

    @Override
    protected void loadEntityType(XmlPullParser parser, Resources resources, Context context) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, getEntityName());
        parser.next();

        String name = "";
        int id = 0;
        int imageId = -1;
        boolean consumable = false;

        int value = 0;
        int weight = 0;
        int addHp = 0;


        while (!parser.getName().equals(getEntityName())) {
            String tag = parser.getName();
            Log.v("ItemLoader.loadEntityType", "Tag: " + parser.getName());

            switch (tag) {
                case "id":
                    id = resources.getIdentifier(parseField(parser), "string", context.getPackageName());
                    name = resources.getString(id);
                break;

                case "image":
                    imageId = resources.getIdentifier(parseField(parser), "drawable", context.getPackageName());
                break;

                case "value":
                    value = Integer.parseInt(parseField(parser));
                break;

                case "weight":
                    weight = Integer.parseInt(parseField(parser));
                break;

                case "consumable":
                    consumable = parseField(parser).equals("true");
                    break;

                case "addHp":
                    addHp = Integer.parseInt(parseField(parser));
                    break;

                default:
                    Log.e("ItemLoader.loadEntityType", "unhandled field: " + parser.getName());
                break;
            }
        }

        if (id == 0) {
            throw new IOException("Id field missing");
        }

        addEntity(id, new ItemType(name, id, imageId, value, weight, addHp, consumable));
    }
}
