package com.enderwolf.dev.georoguelike;


import android.app.Fragment;

/*
 * This fragment is displayed in landscape mode when the location is not yet loaded and we can't
 * display player or map fragment.
 */
import com.enderwolf.dev.georoguelike.utility.IUpdate;

/**
 * This Fragment is displayed when other fragments are waitng on data being loaded
 */
public class LoadingFragment extends Fragment implements IUpdate {

    @Override
    public void update() {
    }
}
