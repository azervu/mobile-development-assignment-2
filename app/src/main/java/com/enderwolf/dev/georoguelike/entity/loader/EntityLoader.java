package com.enderwolf.dev.georoguelike.entity.loader;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;
import android.util.SparseArray;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Azervu on 25.09.2014.
 *
 * Abstract generic loader for any types of game Entities defined in xml.
 */
abstract public class EntityLoader<T extends EntityType> {
    private final SparseArray<T> entityTypes;
    private final Vector<Integer> entityIds;

    EntityLoader(Resources resources, Context context) {
        entityTypes = new SparseArray<>();
        entityIds = new Vector<>();
        loadEntityClass(resources, context);
    }

    abstract protected String getEntityName();

    abstract protected XmlResourceParser getXmlResourceParser(Resources resources);

    abstract protected void loadEntityType(XmlPullParser parser, Resources resources, Context context) throws IOException, XmlPullParserException;

    public T getEntityType(int id) {
        return entityTypes.get(id);
    }

    public int getRandomEntityTypeId() {
        Random random = new Random();
        Log.v("EntityLoader.getRandomEntityTypeId", "entityIds.size(): " + String.valueOf(entityIds.size()));
        return entityIds.elementAt(random.nextInt(entityIds.size()));
    }

    String parseField(XmlPullParser parser) throws XmlPullParserException, IOException {
        String tag = parser.getName();
        String fieldValue;
        parser.require(XmlPullParser.START_TAG, null, tag);
        parser.next();
        fieldValue = parser.getText();
        Log.v("EntityLoader.parseField", "Text: " + fieldValue);
        parser.next();
        parser.require(XmlPullParser.END_TAG, null, tag);
        parser.next();
        return fieldValue;
    }

    void addEntity(int id, T entity) {
        entityTypes.put(id, entity);
        entityIds.add(id);
    }

    private void loadEntityClass(Resources resources, Context context) {
        Log.i("EntityLoader.loadEntityClass", "parsing " + getEntityName());
        XmlResourceParser parser = getXmlResourceParser(resources);
        try {
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_TAG) {
                if (eventType == XmlPullParser.START_TAG && parser.getName().equals(getEntityName())) {
                    loadEntityType(parser, resources, context);
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
    }
}