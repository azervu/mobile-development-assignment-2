package com.enderwolf.dev.georoguelike.entity;

import java.io.Serializable;

/**
 * Created by !Tulingen on 30.09.2014.
 * Stores the inventory and stats.
 */
public class Player implements Serializable {

    private final Stats stats;
    private final Inventory inventory;

    public Player () {
        this.stats = new Stats();
        this.inventory = new Inventory();

        this.stats.setArmor(3);
        this.stats.setDamage(5);
    }

    public boolean addToInventory (Item item) {
        return this.inventory.add(item);
    }

    public Stats getStats() {
        return stats;
    }

    public Inventory getInventory() {
        return inventory;
    }
}

