package com.enderwolf.dev.georoguelike.entity.loader;

import com.enderwolf.dev.georoguelike.entity.Stats;

/**
 * Created by Azervu on 25.09.2014.
 * This class stores data about specific types of items found in the game.
 * Actual instances of these items hold an id reference to to their specific itemtype through the
 * ItemLoader singleton.
 */
public class ItemType extends EntityType {

    private final int value;
    private final int weight;
    private final int addHp;
    private final boolean consumable;

    public ItemType(String name, int id, int imageId, int value, int weight, int addHp, boolean consumable) {
        super(name, id, imageId);

        this.value = value;
        this.weight = weight;
        this.consumable = consumable;
        this.addHp = addHp;
    }

    public boolean interact( Stats stats) {
        if(stats.getHealth() < stats.getMaxHealth()) {
            stats.addHealth(addHp);
            return this.consumable;
        }

        return false;
    }

    public int getAddHp() {
        return addHp;
    }

    public int getValue() {
        return value;
    }

    public int getWeight() {
        return weight;
    }

}