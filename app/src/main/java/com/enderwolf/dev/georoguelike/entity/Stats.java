package com.enderwolf.dev.georoguelike.entity;

import java.io.Serializable;

/**
 * Created by !Tulingen on 30.09.2014.
 * Contains stats
 */
@SuppressWarnings("ALL")
public class Stats implements Serializable {
    private int health;
    private final int maxHealth;

    private int mana;
    private final int maxMana;

    private int armor;
    private int damage;

    public Stats () {
        this.health = this.maxHealth = 100;
        this.mana = this.maxMana = 100;
        this.armor = 0;
        this.damage = 0;
    }

    public Stats (int maxHealth, int maxMana, int armor, int damage) {
        this.health = this.maxHealth = maxHealth;
        this.mana = this.maxMana = maxMana;
        this.armor = armor;
        this.damage = damage;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void addHealth(int healthDelta) {
        this.health += healthDelta;
        this.health = Math.min(this.health, this.maxHealth);
        this.health = Math.max(this.health, 0);
    }

    public int getMaxHealth() {
        return this.maxHealth;
    }


    public int getMana() {
        return this.mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void addMana(int manaDelta) {
        this.mana += manaDelta;
        this.mana = Math.min(this.mana, this.maxMana);
        this.mana = Math.max(this.mana, 0);
    }

    public int getMaxMana() {
        return this.maxMana;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
