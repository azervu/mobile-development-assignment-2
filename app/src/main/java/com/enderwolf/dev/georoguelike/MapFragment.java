package com.enderwolf.dev.georoguelike;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.enderwolf.dev.georoguelike.entity.Terrain;
import com.enderwolf.dev.georoguelike.entity.WorldMap;
import com.enderwolf.dev.georoguelike.entity.loader.TerrainLoader;
import com.enderwolf.dev.georoguelike.entity.loader.TerrainType;
import com.enderwolf.dev.georoguelike.utility.IUpdate;
import com.enderwolf.dev.georoguelike.utility.LatLngHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;


/**
 * A simple {@link Fragment} subclass.
 * This fragment serves as a map display.
 */
public class MapFragment extends Fragment implements IUpdate {

    private static final float DEFAULT_START_ZOOM = 15.0f;

    private WorldMap map = null;

    private MapView googleMapView;
    private GoogleMap googleMap;

    public MapFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.map = ((GameActivity) this.getActivity()).getMap();

        MapsInitializer.initialize(this.getActivity());

        googleMapView = new MapView(this.getActivity());

        googleMapView.onCreate(savedInstanceState);
        googleMapView.setClickable(false);
        googleMapView.setOnClickListener(null);
        googleMapView.setOnDragListener(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout root = (LinearLayout) inflater.inflate(R.layout.fragment_map, container, false);

        root.addView(googleMapView);

        return root;
    }

    @Override
    public void onStart() {
        this.getGoogleMap();

        super.onStart();
    }

    @Override
    public void onResume() {
        googleMapView.onResume();
        this.update();

        super.onResume();
    }

    @Override
    public void onPause() {
        googleMapView.onPause();

        super.onPause();
    }

    @Override
    public void onDestroy() {
        googleMapView.onDestroy();

        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        googleMapView.onSaveInstanceState(outState);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        googleMapView.onLowMemory();

        super.onLowMemory();
    }

    public void update() {
        if(!this.getGoogleMap() || !this.isResumed()) {
            return;
        }

        this.map = ((GameActivity) this.getActivity()).getMap();

        this.googleMap.clear();

        for (Terrain square : this.map.getMap()) {
            TerrainType type = TerrainLoader.GetTerrainLoader().getEntityType(square.getTypeId());
            GroundOverlayOptions overlay = new GroundOverlayOptions();
            overlay.bearing(0.0f);

            LatLng latLng = new LatLng(this.map.getAnchorLat(), this.map.getAnchorLng());
            latLng = LatLngHelper.addLatKM(latLng, square.getPosition().y * WorldMap.SQUARE_SIZE_KM);
            latLng = LatLngHelper.addLngKM(latLng, square.getPosition().x * WorldMap.SQUARE_SIZE_KM);

            overlay.position(latLng, WorldMap.SQUARE_SIZE_M, WorldMap.SQUARE_SIZE_M);
            overlay.image(BitmapDescriptorFactory.fromResource((square.getVisited()) ? type.getImageId() : R.drawable.square_translucent));

            googleMap.addGroundOverlay(overlay);
        }

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.map.getAnchorLat(), this.map.getAnchorLng()), DEFAULT_START_ZOOM));
    }

    private boolean getGoogleMap() {
        if(googleMapView != null) {
            googleMap = googleMapView.getMap();

            if(googleMap == null) {
                return false;
            }

            googleMap.setMyLocationEnabled(true);

            return true;
        }

        return (googleMap != null);
    }
}
