package com.enderwolf.dev.georoguelike.entity.loader;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

import com.enderwolf.dev.georoguelike.utility.LootTable;
import com.enderwolf.dev.georoguelike.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by Azervu on 25.09.2014.
 * This class is a singleton that loads and stores terrain data.
 */
public class TerrainLoader extends EntityLoader<TerrainType> {
    private static TerrainLoader TERRAIN_LOADER = null;

    public static void InitTerrainLoader(Resources resources, Context context) {
        TERRAIN_LOADER = new TerrainLoader(resources, context);
    }

    public static TerrainLoader GetTerrainLoader() {
        if(!IsInit()) {
            Log.e("TerrainLoader.GetTerrainLoader", "function called before init");
            throw new NullPointerException("TERRAIN_LOADER uninitialized");
        }

        return TERRAIN_LOADER;
    }

    private static boolean IsInit () {
        return (TERRAIN_LOADER != null);
    }

    private TerrainLoader(Resources resources, Context context) {
        super(resources, context);
    }

    @Override
    protected String getEntityName() {
        return "terrain";
    }

    @Override
    protected XmlResourceParser getXmlResourceParser(Resources resources) {
        return resources.getXml(R.xml.terrain_types);
    }

    @Override
    protected void loadEntityType(XmlPullParser parser, Resources resources, Context context) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, getEntityName());
        parser.next();

        String name = "";
        int id = 0;
        int imageId = -1;

        LootTable lootTable = null;

        while (!parser.getName().equals(getEntityName())) {
            String tag = parser.getName();
            Log.v("TerrainLoader.loadEntityType", "Tag: " + parser.getName());

            switch (tag) {
                case "id":
                    id = resources.getIdentifier(parseField(parser), "string", context.getPackageName());
                    name = resources.getString(id);
                break;

                case "image":
                    imageId = resources.getIdentifier(parseField(parser), "drawable", context.getPackageName());
                break;

                case "lootTable":
                    lootTable = loadDropTable(parser, resources, context);
                break;

                default:
                    Log.e("TerrainLoader.loadEntityType", "unhandled field: " + parser.getName());
                break;
            }
        }
        if (lootTable == null || id == 0) {
            throw new XmlPullParserException("Missing xml field in terrain_types");
        }

        addEntity(id, new TerrainType(name, id, imageId, lootTable));
    }

    private LootTable loadDropTable(XmlPullParser parser, Resources resources, Context context) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "lootTable");
        parser.next();
        LootTable lootTable = new LootTable();

        while (!parser.getName().equals("lootTable")) {
            parser.require(XmlPullParser.START_TAG, null, "item");
            parser.next();

            int id = 0;
            int dropRate = 1;
            int dropMin = 1;
            int dropMax = 1;

            while (!parser.getName().equals("item")) {
                String tag = parser.getName();
                switch (tag) {
                    case "id":
                        id = resources.getIdentifier(parseField(parser), "string", context.getPackageName());
                    break;

                    case "dropRate":
                        dropRate = Integer.parseInt(parseField(parser));
                    break;

                    case "dropMin":
                        dropMin = Integer.parseInt(parseField(parser));
                    break;

                    case "dropMax":
                        dropMax = Integer.parseInt(parseField(parser));
                    break;

                    default:
                        Log.e("unhandled field: " , parser.getName());
                    break;
                }

            }

            lootTable.addEntry(id, dropRate, dropMin, dropMax);
            parser.require(XmlPullParser.END_TAG, null, "item");

            parser.next();

        }
        parser.require(XmlPullParser.END_TAG, null, "lootTable");
        parser.next();
        return lootTable;
    }
}
