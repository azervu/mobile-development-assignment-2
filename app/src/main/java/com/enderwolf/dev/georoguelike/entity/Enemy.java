package com.enderwolf.dev.georoguelike.entity;

import android.content.Context;

import com.enderwolf.dev.georoguelike.R;
import com.enderwolf.dev.georoguelike.entity.loader.EnemyLoader;
import com.enderwolf.dev.georoguelike.entity.loader.EnemyType;

import java.io.Serializable;

/**
 * Represent an enemy
 * Created by !Tulingen on 30.09.2014.
 */
public class Enemy implements Serializable, IContent {

    private final Stats stats;
    private final int id;

    public Enemy (int id) {
        this.id = id;

        EnemyType type = EnemyLoader.GetEnemyLoader().getEntityType(this.id);

        stats = new Stats(type.getHealth(), type.getMana(), type.getArmor(), type.getDamage());
    }

    @Override
    public InteractResult interact(Player player, Context context) {
        Stats ps = player.getStats();

        boolean enemyAlive = true;
        boolean playerAlive = true;

        int rounds = 0;
        while(enemyAlive && playerAlive && rounds < 10) {
            int enemyDamaged = ps.getDamage() + rounds - this.stats.getArmor();
            int playerDamaged = this.stats.getDamage() + rounds - ps.getArmor();

            this.stats.addHealth(-enemyDamaged);
            ps.addHealth(-playerDamaged);

            enemyAlive = (this.stats.getHealth() > 0);
            playerAlive = (ps.getHealth() > 0);

            rounds++;
        }

        EnemyType type = EnemyLoader.GetEnemyLoader().getEntityType(this.id);


        @SuppressWarnings("StringBufferReplaceableByString") StringBuilder message = new StringBuilder();
        message.append(context.getString(R.string.battle_result_p1));
        message.append(type.getName());
        message.append(context.getString(R.string.battle_result_p2));
        message.append((enemyAlive) ? context.getString(R.string.battle_result_p3_true) : context.getString(R.string.battle_result_p3_false));

        boolean alive = playerAlive;

        return new InteractResult(message.toString(), alive);
    }
}
