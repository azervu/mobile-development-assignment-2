package com.enderwolf.dev.georoguelike.entity;

import android.content.Context;

import com.enderwolf.dev.georoguelike.R;
import com.enderwolf.dev.georoguelike.entity.loader.ItemLoader;
import com.enderwolf.dev.georoguelike.utility.LootTable;

import java.io.Serializable;

/**
 * Created by !Tulingen on 24.09.2014.
 * The base class for instances of objects
 */
public class Item implements Serializable, IContent {

    private final int id;
    private int stackSize;

    public Item (int id, int stackSize) {
        this.id = id;
        this.stackSize = stackSize;
    }

    public Item (LootTable.LootDrop loot) {
        this.id = loot.itemId;
        this.stackSize = loot.amount;
    }

    public int getId() {
        return id;
    }

    public int getStackSize() {
        return stackSize;
    }

    public void addToStackSize(int add) {
        stackSize += add;
    }

    public boolean removeFromStackSize(int remove) {
        if ( stackSize >= remove ) {
            stackSize -= remove;
            return true;
        }
        return false;
    }

    @Override
    public InteractResult interact(Player player, Context context) {
        boolean possible = player.addToInventory(this);
        StringBuilder message = new StringBuilder();

        if(possible) {
            message.append(context.getString(R.string.item_pickup_true));
        } else {
            message.append(context.getString(R.string.item_pickup_false));
        }

        message.append(this.stackSize);
        message.append(" x ");
        message.append(ItemLoader.GetItemLoader().getEntityType(this.id).getName());

        return new InteractResult(message.toString(), possible);
    }
}

