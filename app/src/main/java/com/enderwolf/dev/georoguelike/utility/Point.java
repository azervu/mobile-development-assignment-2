package com.enderwolf.dev.georoguelike.utility;

import android.support.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by !Tulingen on 30.09.2014.
 * A helper class storing X y coordinates.
 */
public class Point implements Serializable, Comparable {

    public final int x;
    public final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    @Override
    public boolean equals(Object o) {
        return (o instanceof Point && ((Point) o).x == this.x && ((Point) o).y == this.y);
    }


    @Override
    public String toString() {
        return (this.x + ":" + this.y);
    }

    @Override
    public int compareTo(@NonNull Object another) {
        if(this.equals(another)) {
            return 0;
        } else if (this.x > ((Point) another).x) {
            return 1;
        } else if (this.x == ((Point) another).x){
            return (this.y > ((Point) another).y) ? 1 : -1;
        } else {
            return -1;
        }
    }
}
