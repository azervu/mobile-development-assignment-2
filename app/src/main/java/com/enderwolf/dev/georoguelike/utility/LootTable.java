package com.enderwolf.dev.georoguelike.utility;

import java.util.Random;
import java.util.Vector;

/**
 * Created by Azervu on 25.09.2014.
 * Stores possible loot drops in a specific terrain type and generates random drops for it.
 */
public class LootTable{
    private int totalDropRate;
    private final Vector<LootEntry> lootEntries;

    public LootTable() {
        lootEntries = new Vector<>();
    }

    public void addEntry(int id, int weight, int min, int max) {
        this.totalDropRate += weight;
        lootEntries.add(new LootEntry(id, weight, min, max));
    }

    public LootDrop getRandomDrop() {
        Random random = new Random();
        final int select = random.nextInt(totalDropRate);
        int index = 0;
        int count = lootEntries.elementAt(index).getDropRate();
        while (count < select) {
            index++;
            count += lootEntries.elementAt(index).getDropRate();
        }
        LootDrop lootDrop = new LootDrop();
        lootDrop.itemId = lootEntries.elementAt(index).getId();
        lootDrop.amount = random.nextInt(1+lootEntries.elementAt(index).getMax()-lootEntries.elementAt(index).getMin())+lootEntries.elementAt(index).getMin();


        return lootDrop;
    }

    public class LootDrop {
        public int amount;
        public int itemId;
    }

    private class LootEntry {

        LootEntry(int id, int dropRate, int min, int max) {
            this.id = id;
            this.max = max;
            this.min = min;
            this.dropRate = dropRate;
        }

        public int getId() {
            return id;
        }

        public int getDropRate() {
            return dropRate;
        }

        public int getMin() {
            return min;
        }

        public int getMax() {
            return max;
        }

        private final int id;
        private final int dropRate;
        private final int min;
        private final int max;
    }
}