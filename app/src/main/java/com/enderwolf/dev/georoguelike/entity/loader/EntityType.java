package com.enderwolf.dev.georoguelike.entity.loader;

import java.io.Serializable;

/**
 * Created by !Tulingen on 30.09.2014.
 * This is the basis for game items types.
 */
@SuppressWarnings("ALL")
public class EntityType implements Serializable {

    private final String name;
    private final int id;
    private final int imageId;

    EntityType(String name, int id, int imageId) {
        this.name = name;
        this.id = id;
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getImageId() {
        return imageId;
    }
}
