package com.enderwolf.dev.georoguelike;

import android.app.Activity;
import android.app.Fragment;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.enderwolf.dev.georoguelike.entity.IContent;
import com.enderwolf.dev.georoguelike.entity.Item;
import com.enderwolf.dev.georoguelike.entity.Terrain;
import com.enderwolf.dev.georoguelike.entity.Player;
import com.enderwolf.dev.georoguelike.entity.WorldMap;
import com.enderwolf.dev.georoguelike.entity.loader.EnemyLoader;
import com.enderwolf.dev.georoguelike.entity.loader.ItemLoader;
import com.enderwolf.dev.georoguelike.entity.loader.TerrainLoader;
import com.enderwolf.dev.georoguelike.utility.IUpdate;
import com.enderwolf.dev.georoguelike.utility.SaveSystem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

/**
 *
 * This class loads the rest of the application and provides the UI for switching between the player
 * and explore fragments
 */

public class GameActivity extends Activity implements
        ExploreFragment.OnContentListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener,
        AdapterView.OnItemClickListener {

    private MapFragment mapFragment = null;
    private ExploreFragment exploreFragment = null;
    private PlayerFragment playerFragment = null;
    private LoadingFragment loadingFragment = null;
    private Fragment activeFragment = null;

    private static final String DEFAULT_PLAYER_SAVE_NAME = "player.save";
    private static final String DEFAULT_MAP_SAVE_NAME = "map.save";

    private static final int LOCATION_FASTEST_INTERVAL = 1000;
    private static final int LOCATION_INTERVAL = 5000;

    private Player player;
    private WorldMap map;

    private boolean dualView = false;

    private Button leftButton;
    private Button rightButton;
    private Button button;

    private Location currentPos = null;

    private LocationClient locationClient = null;
    private LocationRequest locationRequest = null;
    private boolean doNewGame = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Do we even have location service?
        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS) {
            this.getMainLooper().quit();
        }

        locationClient = new LocationClient(this, this, this);
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(LOCATION_FASTEST_INTERVAL);
        locationRequest.setInterval(LOCATION_INTERVAL);

        ItemLoader.InitItemLoader(this.getResources(), this.getBaseContext());
        TerrainLoader.InitTerrainLoader(this.getResources(), this.getBaseContext());
        EnemyLoader.InitEnemyLoader(this.getResources(), this.getBaseContext());

        this.loadData();

        this.mapFragment = new MapFragment();
        this.exploreFragment = new ExploreFragment();
        this.playerFragment = new PlayerFragment();
        this.loadingFragment = new LoadingFragment();

        if (findViewById(R.id.center_container) != null) {

            this.dualView = true;

            this.button = (Button) this.findViewById(R.id.button);
            this.button.setText(R.string.explore);

            getFragmentManager().beginTransaction().replace(R.id.center_container, this.mapFragment).commit();

            setActiveFragment(this.playerFragment);

        } else {

            this.dualView = false;

            this.leftButton = (Button) this.findViewById(R.id.left_button);
            this.leftButton.setText(R.string.explore);

            this.rightButton = (Button) this.findViewById(R.id.right_button);
            this.rightButton.setText(R.string.player);

            setActiveFragment(this.mapFragment);
        }
    }

    @Override
    protected void onStart() {
        this.locationClient.connect();

        super.onStart();
    }

    @Override
    protected void onResume() {
        this.loadData();

        super.onResume();
    }

    @Override
    protected void onPause() {
        this.saveData();

        super.onPause();
    }

    @Override
    protected void onStop() {
        this.locationClient.disconnect();

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (this.dualView) {
            if (this.activeFragment != this.playerFragment) {
                this.setActiveFragment(this.playerFragment);
            } else {
                super.onBackPressed();
            }
        } else {
            if (this.activeFragment != this.mapFragment) {
                this.setActiveFragment(this.mapFragment);
            } else {
                super.onBackPressed();
            }
        }
    }

    private void setActiveFragment(Fragment fragment) {

        if (this.currentPos == null) {
            Toast toast = Toast.makeText(getBaseContext(), R.string.loading, Toast.LENGTH_LONG);
            toast.show();
            if (this.dualView) {
                fragment = this.loadingFragment;
            } else {
                fragment = this.mapFragment;
            }

        }

        if (fragment == this.exploreFragment) {
            if(this.dualView) {
                this.button.setText(R.string.player);
                this.button.setOnClickListener(new ButtonListener(this.playerFragment));
                this.exploreFragment.setCurrentSquare(this.map.getSquare(this.currentPos));
            } else {
                this.leftButton.setText(R.string.map);
                this.leftButton.setOnClickListener(new ButtonListener(this.mapFragment));
                this.rightButton.setText(R.string.player);
                this.rightButton.setOnClickListener(new ButtonListener(this.playerFragment));
                this.exploreFragment.setCurrentSquare(this.map.getSquare(this.currentPos));
            }
        } else if (fragment == this.playerFragment) {
            if(this.dualView) {
                this.button.setText(R.string.explore);
                this.button.setOnClickListener(new ButtonListener(this.exploreFragment));
            } else {
                this.rightButton.setText(R.string.map);
                this.rightButton.setOnClickListener(new ButtonListener(this.mapFragment));
                this.leftButton.setText(R.string.explore);
                this.leftButton.setOnClickListener(new ButtonListener(this.exploreFragment));
            }
        } else if (fragment == this.mapFragment) {
            this.rightButton.setText(R.string.player);
            this.leftButton.setText(R.string.explore);
            this.leftButton.setOnClickListener(new ButtonListener(this.exploreFragment));
            this.rightButton.setOnClickListener(new ButtonListener(this.playerFragment));

        } else if (fragment == this.loadingFragment) {
            this.button.setText(R.string.explore);
            this.button.setOnClickListener(new ButtonListener(this.exploreFragment));
        }

        this.activeFragment = fragment;
        getFragmentManager().beginTransaction().replace(R.id.pane_container, fragment).commit();
    }

    public void updateFragments() {
        if(this.dualView) {
            this.mapFragment.update();
        }

        ((IUpdate) this.activeFragment).update();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.game, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_game:
                this.doNewGame = true;
                this.tryNewGame();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public Player getPlayer() {
        return this.player;
    }

    public WorldMap getMap() {
        return this.map;
    }

    public Terrain getCurrentSquare() {
        return this.map.getSquare(this.currentPos);
    }

    private void tryNewGame() {
        if(this.doNewGame && this.currentPos != null) {
            this.newGame();
            this.doNewGame = false;
        }
    }

    private void newGame() {
        this.map.generateNewWorld(this.currentPos);
        this.player = new Player();
        this.saveData();

        this.updateFragments();
    }

    private void loadData() {
        this.map = SaveSystem.LoadData(DEFAULT_MAP_SAVE_NAME, this.getBaseContext());
        this.player = SaveSystem.LoadData(DEFAULT_PLAYER_SAVE_NAME, this.getBaseContext());

        if(this.map == null || this.player == null) {
            this.map = new WorldMap();
            this.player = null;

            this.doNewGame = true;
            this.tryNewGame();
        }
    }

    private void saveData() {
        SaveSystem.SaveData(DEFAULT_MAP_SAVE_NAME, this.getBaseContext(), this.map);
        SaveSystem.SaveData(DEFAULT_PLAYER_SAVE_NAME, this.getBaseContext(), this.player);
    }

    @Override
    public IContent.InteractResult onContentFound(IContent content) {
        IContent.InteractResult result = content.interact(this.player, this);

        Toast.makeText(this, result.getMessage(), Toast.LENGTH_SHORT).show();

        this.updateFragments();

        return result;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Item item = player.getInventory().get(position);

        if(ItemLoader.GetItemLoader().getEntityType(item.getId()).interact(player.getStats())) {
            player.getInventory().removeItem(item.getId(), 1);
        }

        this.playerFragment.update();
    }

    private class ButtonListener implements View.OnClickListener {
        private final Fragment fragment;

        ButtonListener(Fragment fragment) {
            this.fragment = fragment;
        }

        @Override
        public void onClick(View view) {
            setActiveFragment(fragment);
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        locationClient.requestLocationUpdates(locationRequest, this);

        this.tryNewGame();
    }

    @Override
    public void onDisconnected() {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Based on http://developer.android.com/training/location/receive-location-updates.html#DefineCallbacks
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, connectionResult.getErrorCode());

                //Thrown if Google Play services canceled the original PendingIntent
            } catch (IntentSender.SendIntentException e) {
                Log.e("onConnectionFailed", "Google Play services most likely canceled PendingIntent", e);
            }
        } else {
            Log.e("onConnectionFailed", "Unrecoverable error with location service");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(this.currentPos == null) {
            this.currentPos = location;
            this.tryNewGame();
        } else {
            this.currentPos = location;
        }

        if(this.activeFragment == this.exploreFragment) {
            this.exploreFragment.setCurrentSquare(this.getCurrentSquare());
        }
    }
}
