package com.enderwolf.dev.georoguelike.entity;

import android.content.Context;

import java.io.Serializable;

/**
 * Interface for content in Terrain
 * Created by !Tulingen on 24.09.2014.
 */
public interface IContent extends Serializable {
    /**
     * An interaction between a player and stuff
     * @param player the player interacting
     * @return true if it should be removed from the game world, false otherwise
     */
    public InteractResult interact(Player player, Context context);

    class InteractResult {
        private final String message;
        private final boolean result;

        public InteractResult (String message, boolean result) {
            this.message = message;
            this.result = result;
        }

        public String getMessage () {
            return this.message;
        }

        public boolean getResult () {
            return this.result;
        }
    }
}
