package com.enderwolf.dev.georoguelike.entity;

import android.location.Location;

import com.enderwolf.dev.georoguelike.entity.loader.EnemyLoader;
import com.enderwolf.dev.georoguelike.utility.GridMap;
import com.enderwolf.dev.georoguelike.utility.Point;
import com.enderwolf.dev.georoguelike.entity.loader.TerrainLoader;
import com.enderwolf.dev.georoguelike.utility.LatLngHelper;
import com.enderwolf.dev.georoguelike.utility.LootTable;

import java.io.Serializable;
import java.util.Random;

/**
 * Created by !Tulingen on 30.09.2014.
 * Stores Terrain.
 */
@SuppressWarnings("ALL")
public class WorldMap implements Serializable {

    public static final float SQUARE_SIZE_M = 150.0f;
    public static final double SQUARE_SIZE_KM = SQUARE_SIZE_M * 0.001;

    private static final Point [] THREE_BY_THREE = {
            new Point(-1, -1),
            new Point( 0, -1),
            new Point( 1, -1),

            new Point(-1,  0),
            new Point( 0,  0),
            new Point( 1,  0),

            new Point(-1,  1),
            new Point( 0,  1),
            new Point( 1,  1)
    };

    private GridMap<Terrain> map = new GridMap<>();
    private double anchorLat = 0.0;
    private double anchorLng = 0.0;

    public WorldMap () {

    }

    public double getAnchorLat() {
        return anchorLat;
    }

    public double getAnchorLng() {
        return anchorLng;
    }

    public GridMap<Terrain> getMap() {
        return map;
    }

    public Terrain getSquare(Location location) {

        double kmDeltaY = LatLngHelper.latToKM(location.getLatitude()) - LatLngHelper.latToKM(this.anchorLat);
        double kmDeltaX = LatLngHelper.lngToKM(location.getLatitude(), location.getLongitude()) - LatLngHelper.lngToKM(this.anchorLat, this.anchorLng);

        int x = (int) Math.round(kmDeltaX / SQUARE_SIZE_KM);
        int y = (int) Math.round(kmDeltaY / SQUARE_SIZE_KM);

        this.generateSurroundingTerrain(x, y);

        return this.map.getNode(x, y);
    }


    public void generateNewWorld (Location location) {
        this.map = new GridMap<>();
        this.anchorLat = location.getLatitude();
        this.anchorLng = location.getLongitude();

        this.generateSurroundingTerrain(0, 0);
    }

    private void generateSurroundingTerrain (int x, int y) {
        for (Point p : THREE_BY_THREE) {
            int xPos = x + p.x;
            int yPos = y + p.y;

            if(!map.exists(xPos, yPos)) {
                generateTerrain(xPos, yPos);
            }
        }
    }

    /**
     * Generates a new map square and automatically adds it to the map
     * @param x The x coordinate
     * @param y The y coordinate
     */
    private void generateTerrain (int x, int y) {
        Terrain square = new Terrain(new Point(x, y), TerrainLoader.GetTerrainLoader().getRandomEntityTypeId());

        Random r = new Random();
        int stuff = r.nextInt(8);

        for (int i = 0; i < stuff; i++) {
            if(r.nextInt(4) == 0) {
                square.getContent().add(new Enemy(EnemyLoader.GetEnemyLoader().getRandomEntityTypeId()));
            } else {
                LootTable.LootDrop loot = TerrainLoader.GetTerrainLoader().getEntityType(square.getTypeId()).getRandomDrop();
                square.getContent().add(new Item(loot));
            }
        }

        this.map.addNode(square);
    }
}
