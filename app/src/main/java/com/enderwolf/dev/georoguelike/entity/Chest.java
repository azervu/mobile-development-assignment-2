package com.enderwolf.dev.georoguelike.entity;

import android.content.Context;

import java.io.Serializable;

/**
 * Represent a chest
 * Created by !Tulingen on 24.09.2014.
 */
@SuppressWarnings("ALL")
public class Chest implements Serializable, IContent {
    private final Inventory content;

    public Chest () {
        this.content = new Inventory();
    }

    public Chest (Item ... items) {
        this.content = new Inventory(items);
    }

    @Override
    public InteractResult interact(Player player, Context context) {

        // TODO implement properly
        return new InteractResult("", false);
    }
}
