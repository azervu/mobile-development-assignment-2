package com.enderwolf.dev.georoguelike.utility;

import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * Euler coordinate based storage
 * Created by !Tulingen on 23.09.2014.
 */
@SuppressWarnings("ALL")
public class GridMap <T extends GridMap.Node & Serializable> implements Iterable<T>, Serializable {

    private final TreeMap<Point, T> map;

    public GridMap () {
        map = new TreeMap<>();
    }

    public void addNode (T node) {
        map.put(node.getPosition(), node);
    }

    public boolean exists (Point position) {
        return map.containsKey(position);
    }

    public boolean exists (int x, int y) {
        return this.exists(new Point(x, y));
    }

    public T getNode (Point position) {
        return map.get(position);
    }

    public T getNode (int x, int y) {
        return this.getNode(new Point(x, y));
    }

    public T removeNode (Point position) {
        return map.remove(position);
    }

    public T removeNode (int x, int y) {
        return this.removeNode(new Point(x, y));
    }

    @Override
    public Iterator<T> iterator() {
        return map.values().iterator();
    }

    public interface Node {
        public Point getPosition();
    }
}