package com.enderwolf.dev.georoguelike.utility;

/**
 * Created by !Tulingen on 01.10.2014.
 * The interface for the fragment update
 */
public interface IUpdate {
    public void update ();
}
