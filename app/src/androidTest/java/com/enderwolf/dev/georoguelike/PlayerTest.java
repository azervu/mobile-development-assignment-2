package com.enderwolf.dev.georoguelike;

import android.test.ActivityTestCase;

import com.enderwolf.dev.georoguelike.entity.Item;
import com.enderwolf.dev.georoguelike.entity.Player;

public class PlayerTest extends ActivityTestCase {

    private Player player;
    private Item testItem;

    public void setUp() throws Exception {
        player = new Player();
        testItem = new Item(1, 10001);
    }

    public void testAddToInventory() throws Exception {

        assertEquals(player.addToInventory(testItem), true);
        assertEquals(player.addToInventory(testItem), false);
    }
}