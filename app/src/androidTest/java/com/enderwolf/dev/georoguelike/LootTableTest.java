package com.enderwolf.dev.georoguelike;

import com.enderwolf.dev.georoguelike.utility.LootTable;

import junit.framework.TestCase;

public class LootTableTest extends TestCase {
    private LootTable lootTableA;
    private LootTable lootTableB;

    public void setUp() throws Exception {
        lootTableA = new LootTable();
        lootTableA.addEntry(9, 0, 1, 1);
        lootTableA.addEntry(17, 100, 1, 1);
        lootTableB = new LootTable();
        lootTableB.addEntry(9, 1, 1, 1);
    }


    public void testGetRandomDropId() throws Exception {
        assertEquals(17, lootTableA.getRandomDrop().itemId);

        assertEquals(9, lootTableB.getRandomDrop().itemId);
    }
}